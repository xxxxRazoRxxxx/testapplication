package com.example.testapplicateion

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.example.testapplicateion.fragments.FragmentListOfPosts
import com.example.testapplicateion.fragments.FragmentPostContent



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fragmentListOfPosts = FragmentListOfPosts()

        val fragmentTrans = supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, fragmentListOfPosts)
            .commit()
    }
}
