package com.example.testapplicateion.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testapplicateion.R
import com.example.testapplicateion.classes.Post
import com.example.testapplicateion.fragments.FragmentListOfPosts
import com.example.testapplicateion.fragments.FragmentPostContent

class PostsAdapter(var items: List<Post>, private val fragment: FragmentListOfPosts) : RecyclerView.Adapter<PostsAdapter.PostHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = PostHolder(LayoutInflater.from(parent.context).inflate(R.layout.fragment_list_of_posts_element, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class PostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val postTitle = itemView.findViewById<TextView>(R.id.post_title)
        private val postCover = itemView.findViewById<ImageView>(R.id.post_cat_cover)

        fun bind(item: Post) {
            postTitle.text = item.title.rendered

            Glide.with(itemView)
                .load(item.cat_cover.sizes.mobile)
                .placeholder(R.drawable.ic_no_image)
                .fitCenter()
                .into(postCover)

            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) onItemClicked(items[adapterPosition])
            }
        }
    }

    fun onItemClicked(item: Post){
        val bundle = Bundle()
        bundle.putString("Title", item.title.rendered)
        bundle.putString("Content", item.content.rendered)

        val fragmentPostContent = FragmentPostContent()
        val fragmentTrans = fragment.activity!!.supportFragmentManager

        fragmentPostContent.arguments = bundle

        fragmentTrans.beginTransaction()
            .replace(R.id.main_container, fragmentPostContent)
            .addToBackStack(null)
            .commit()
    }
}