package com.example.testapplicateion.api

import com.example.testapplicateion.classes.Post
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

interface LifeHackerApi {

    @GET("posts/")
    fun getPosts(): Call<List<Post>>

    companion object Factory {
        fun create(): LifeHackerApi {

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val getRequestHeader = OkHttpClient.Builder()
                .connectTimeout(20.toLong(), TimeUnit.SECONDS)
                .writeTimeout(20.toLong(), TimeUnit.SECONDS)
                .readTimeout(20.toLong(), TimeUnit.SECONDS)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl("https://lifehacker.ru/api/wp/v2/")
                .client(getRequestHeader)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            return retrofit.create(LifeHackerApi::class.java)
        }
    }
}