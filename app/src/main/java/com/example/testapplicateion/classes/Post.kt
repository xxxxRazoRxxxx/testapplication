package com.example.testapplicateion.classes

import com.google.gson.annotations.SerializedName

class Post{
    @SerializedName("id")
    val id: Int = 0
    @SerializedName("title")
    val title: Content = Content()
    @SerializedName("content")
    val content: Content = Content()
    @SerializedName("cat_cover")
    val cat_cover: Cover = Cover()
}

class Content{
    @SerializedName("rendered")
    val rendered: String = ""
}

class Cover {
    @SerializedName("sizes")
    val sizes: Sizes = Sizes()
}

class Sizes{
    @SerializedName("mobile")
    val mobile: String = ""
}

