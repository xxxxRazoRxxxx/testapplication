package com.example.testapplicateion.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import com.example.testapplicateion.R
import com.example.testapplicateion.adapters.PostsAdapter
import com.example.testapplicateion.classes.Post
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testapplicateion.api.LifeHackerApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import androidx.appcompat.app.AppCompatActivity



class FragmentListOfPosts : Fragment() {

    private var appBar: ActionBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_list_of_posts, container, false)

        appBar = (activity as AppCompatActivity).supportActionBar

        val postsViewer = view.findViewById<RecyclerView>(R.id.posts_list)
        val progressBar = view.findViewById<ProgressBar>(R.id.progressBar)

        val apiService = LifeHackerApi.Factory.create()
        val fragment = this

        val llm = LinearLayoutManager(view.context)

        progressBar.visibility = View.VISIBLE
        appBar!!.title = resources.getString(R.string.app_name)

        postsViewer.layoutManager = llm

        apiService.getPosts().enqueue(object : Callback<List<Post>> {
            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                if (response.isSuccessful() && response.body() != null) {
                    val myAdapter = PostsAdapter(response.body()!!, fragment)

                    progressBar.visibility = View.GONE
                    postsViewer.adapter = myAdapter
                } else {
                    Toast.makeText(activity, "Bad response", Toast.LENGTH_SHORT)
                }
            }

            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                 Toast.makeText(activity, "Connection error", Toast.LENGTH_SHORT)
            }
        })

        return view
    }

    override fun onDetach() {
        super.onDetach()
    }
}
