package com.example.testapplicateion.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.TextView

import com.example.testapplicateion.R
import com.example.testapplicateion.classes.Post
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_post_content.*
import android.opengl.ETC1.getWidth
import android.os.Build
import androidx.core.content.ContextCompat.getSystemService
import android.view.WindowManager
import android.view.Display
import android.webkit.WebSettings
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity

class FragmentPostContent : Fragment() {


    private var title: String? = null
    private var content: String? = null

    private var webView: WebView? = null
    private var appBar: ActionBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = it.getString("Title")
            content = it.getString("Content")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_post_content, container, false)

        appBar = (activity as AppCompatActivity).supportActionBar

        webView = view.findViewById(R.id.webView)

        openData()

        return view
    }

    override fun onDetach() {
        super.onDetach()
    }

    private fun openData(){
        appBar!!.title = title

        webView!!.settings.javaScriptEnabled = true
        webView!!.loadData(getHtmlData(content!!), "text/html; charset=utf-8", "UTF-8")
    }

    private fun getHtmlData(bodyHTML: String): String {
        val head = "<head><style>img{max-width: 100%; width:auto; height: auto;}</style></head>"
        return "<html>$head<body>$bodyHTML</body></html>"
    }
}
